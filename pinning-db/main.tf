provider "kubernetes" {
  config_path    = "~/.kube/config"
}
resource "kubernetes_config_map" "dbconfig" {
   metadata {
       name = "dbconfig"
       namespace = "monitoring"
   }
   data = {
       "pinning-db-init.js" = "${file("init.js")}"
   }
}