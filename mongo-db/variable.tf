variable "cluster-name" {
  description = "Name of the Kubernetes cluster"
  type        = string
  default = "minikube"
}