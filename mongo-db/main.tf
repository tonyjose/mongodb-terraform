locals {
mongodb = {
    name           = "hux-mongodb"
    namespace      = "monitoring"
    repository     = "https://charts.bitnami.com/bitnami"
    chart          = "mongodb"
    chart_version  = "10.10.2"
    enabled        = true
    default_values = local.mongodb_values
    }
    mongodb_values = <<ENDYAML
    fullnameOverride: "hux-mongodb-qwrt"
    auth:
      rootPassword: "user123"
    initdbScripts: 
      my_init_script.js: |
        db = db.getSiblingDB('sample_db');

        db.createCollection('sample_collection');

        db.sample_collection.insertMany([
        {
          org: 'helpdev',
          filter: 'EVENT_A',
          addrs: 'http://rest_client_1:8080/wh'
        },
        {
          org: 'helpdev',
          filter: 'EVENT_B',
          addrs: 'http://rest_client_2:8081/wh'
        },
        {
          org: 'github',
          filter: 'EVENT_C',
          addrs: 'http://rest_client_3:8082/wh'
        }  
        ]);
    ENDYAML
}
module "mongodb-helm" {
  source      = "../helm_deploy"
  helm_config = local.mongodb
}
